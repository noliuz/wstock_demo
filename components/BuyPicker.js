import React, { Component } from 'react';
import {View,Text,TextInput,Button,Picker } from 'react-native';
import config from '../config.js'


export default class BuyPicker extends Component {
  state = {
        buyList: []
  }


  async componentDidMount() {

    try {
      let url = config.API_URL+'/buy/get/-1'
      console.log(url)
      const buyCall = await fetch(url)
      const buys = await buyCall.json()
      this.setState({buyList: buys})


    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  convertDT(dt) {
    var d  = new Date(dt)
    return d.getDate()+'/'+d.getMonth()+'/'+d.getYear()+' '+d.getHours()+'.'+d.getMinutes()+'น.'
  }

  render() {
    //console.log(JSON.stringify(this.state.buyList))
    return(
      <View style={{flex:1,marginTop:100}}>
        <Picker style={{ height: 50, width: 300 }}
        >
          {this.state.buyList.map((item, index) => {
            return (<Picker.Item label={item.symbol+' '+item.price+'฿ '+this.convertDT(item.updatedAt)} value={item.id} key={item.id}/>)
          })}


        </Picker>
      </View>
    )
  }
}
