import React, { Component } from 'react';
import { StyleSheet,View,Image,Text,TextInput,Button } from 'react-native';

export default class BuyPanel extends Component {
  render() {
    return(
      <View style={{flex:1,marginTop:50}}>
        <View>
          <Text >Symbol</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
          />
        </View>
        <View style={{marginTop:20}}>
          <Text >Amount</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
          />
        </View>

        <View style={{marginTop:20}}>
          <Text >Price</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
          />
        </View>

        <View style={{marginTop:20}}>
          <Text >Stop Loss</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
          />
        </View>

        <View style={{marginTop:20}}>
          <Text>Target Price</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
          />
        </View>
        <View style={{marginTop:40}}>
          <Button style={{}} title='Save'/>
        </View>

      </View>
    )
  }
}
