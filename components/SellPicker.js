import React, { Component } from 'react';
import {View,Text,TextInput,Button,Picker } from 'react-native';
import config from '../config.js'


export default class SellPicker extends Component {
  state = {
        sellList: []
  }

  async componentDidMount() {
    try {
      //get sell list
      let url = config.API_URL+'/sell/get/-1'
      console.log(url)
      const sellCall = await fetch(url)
      let sells = await sellCall.json()
      this.setState({sellList: sells})
    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  convertDT(dt) {
    var d  = new Date(dt)
    return d.getDate()+'/'+d.getMonth()+'/'+d.getYear()+' '+d.getHours()+'.'+d.getMinutes()+'น.'
  }

  render() {
    return(
      <View style={{flex:1,marginTop:100}}>
        <Picker style={{ height: 50, width: 300 }}
        >
          {this.state.sellList.map((item, index) => {
            return (<Picker.Item label={item.buy.symbol+' '+item.price+'฿ '+this.convertDT(item.updatedAt)} value={item.id} key={item.id}/>)
          })}


        </Picker>
      </View>
    )
  }
}
