import * as React from 'react';
import { Component } from 'react';
import { StyleSheet,View,Image } from 'react-native';
import {ImageButton} from 'react-native-image-button-text';


export default class LeftMenu extends Component {

  render() {
    return(
      <View style={{flex:1,justifyContent:'space-around'}}>
        <Image
         style={{width:60,height:60}}
         source={require('../assets/buy_butt.png')}
        />
        <ImageButton
         style={{width:60,height:60}}
         width={60}
         height={60}
         text=''
         source={require('../assets/sell_butt.png')}
         onPress={() => navigation.Navigate('SellPanel')}

        />
        <Image
         style={{width:60,height:60}}
         source={require('../assets/open_long_butt.png')}
        />
        <Image
         style={{width:60,height:60}}
         source={require('../assets/close_long_butt.png')}
        />
        <Image
         style={{width:60,height:60}}
         source={require('../assets/open_short_butt.png')}
        />
        <Image
         style={{width:60,height:60}}
         source={require('../assets/close_short_butt.png')}
        />

      </View>
    )
  }
}
