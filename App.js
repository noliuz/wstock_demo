import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Image } from 'react-native';
import LeftMenu from './components/LeftMenu'
import BuyPanel from './components/BuyPanel'
import SellPanel from './components/SellPanel'
import BuyPicker from './components/BuyPicker'
import SellPicker from './components/SellPicker'
import {ImageButton} from 'react-native-image-button-text';


const Stack = createStackNavigator()

export default function App({navigation}) {
  return (

    <View style={styles.container}>
      <View style={{marginLeft:10,marginRight:20}}>
        <ImageButton
         style={{width:60,height:60}}
         width={60}
         height={60}
         text=''
         source={require('./assets/sell_butt.png')}
         onPress={() => navigation.Navigate('SellPanel')}

        />
      </View>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="BuyPanel">
          <Stack.Screen name="BuyPanel" component={BuyPanel} />
          <Stack.Screen name="SellPanel" component={SellPanel} />
        </Stack.Navigator>
      </NavigationContainer>


      <StatusBar style="auto" />
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
});
